package testProject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import testProject.models.EntityTransaction;

interface TransactionRepository extends JpaRepository<EntityTransaction, Long> {

}
