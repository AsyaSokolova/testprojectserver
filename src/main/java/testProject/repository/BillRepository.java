package testProject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import testProject.models.EntityBill;

interface BillRepository extends JpaRepository<EntityBill, Long> {

}

