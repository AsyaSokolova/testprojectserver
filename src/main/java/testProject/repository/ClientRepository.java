package testProject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import testProject.models.EntityClient;

interface ClientRepository extends JpaRepository<EntityClient, Long> {

}

