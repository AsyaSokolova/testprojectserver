package testProject.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.web.bind.annotation.*;
import testProject.models.Client;
import testProject.models.EntityClient;

import javax.servlet.http.HttpServletResponse;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.JdbcTemplate;

@RestController
@RequestMapping(path = "/bank")
public class ClientController {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @GetMapping(value = "/client/{idClient}")
    public Map<String, Object> getClientById(@PathVariable(value = "idClient") Integer idClient){

        String sql = "SELECT * FROM \"itProject\".\"Client\" WHERE \"ID\" = ?";
        return jdbcTemplate.queryForMap(sql, idClient);
    }

    @GetMapping(value = "/clients")
    public List<Map<String, Object>> getClients(){
        String sql = "SELECT * FROM \"itProject\".\"Client\"";
        List<Map<String, Object>> clients = jdbcTemplate.queryForList(sql);
        return clients;
    }

    @PostMapping("/client")
    public int addClient(@RequestBody Client client){
        return jdbcTemplate.update(
                "INSERT INTO \"itProject\".\"Client\" " +
                        "(\"name\", \"age\") VALUES (?, ?)", client.getName(), client.getAge());
    }

}


