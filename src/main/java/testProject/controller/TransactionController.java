package testProject.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.*;
import testProject.models.Client;
import testProject.models.EntityBill;
import testProject.models.Transaction;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(path = "/bank")
public class TransactionController {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @GetMapping(value = "/transactions")
    public List<Map<String, Object>> getTransactions(){
        String sql = "SELECT tr.\"ID\", tr.\"amount\", tr.assignment, " +
                "bill.money, bill.\"ID\" \"billId\", " +
                "client.name \"clientName\", " +
                "client.age \"clientAge\" FROM \"itProject\".\"Transaction\" tr " +
                "LEFT JOIN \"itProject\".\"Bill\" bill " +
                "ON tr.\"billFrom\" = bill.\"ID\"" +
                "LEFT JOIN \"itProject\".\"Client\" client ON bill.\"client\" = client.\"ID\"";

        List<Map<String, Object>> transactions = jdbcTemplate.queryForList(sql);
        return transactions;
    }

    @PostMapping("/transaction")
    public int addTransaction(@RequestBody Transaction transaction){
        if (transaction.getBillTo() == -1){
            return jdbcTemplate.update("INSERT INTO \"itProject\".\"Transaction\" " +
                    "(\"billFrom\", \"amount\", \"assignment\") VALUES (?, ?, ?)", +
                    transaction.getBillFrom(), transaction.getAmount(), transaction.getAssignment());
        }else{
            return jdbcTemplate.update("INSERT INTO \"itProject\".\"Transaction\" " +
                    "(\"billFrom\", \"billTo\", \"amount\", \"assignment\") VALUES (?, ?, ?, ?)", +
                    transaction.getBillFrom(), transaction.getBillTo(), transaction.getAmount(), transaction.getAssignment());
        }
    }
}
