package testProject.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.*;
import testProject.models.Bill;
import testProject.models.BillChangeMoney;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(path = "/bank")
public class BillController {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @GetMapping(value = "/bills/{idClient}")
    public List<Map<String, Object>> getBillsByClient(@PathVariable(value = "idClient") Integer idClient){
        String sql = "SELECT * FROM \"itProject\".\"Bill\" WHERE \"client\" = ?";
        List<Map<String, Object>> bills = jdbcTemplate.queryForList(sql, idClient);
        return bills;
    }

    @PostMapping("/bill")
    public int addBill(@RequestBody Bill bill){
        return jdbcTemplate.update(
                "INSERT INTO \"itProject\".\"Bill\" " +
                        "(\"money\", \"client\") VALUES (?, ?)", bill.getMoney(), bill.getClient());
    }

    @GetMapping(value = "/bills")
    public List<Map<String, Object>> getTransactions(){
        String sql = "SELECT * FROM \"itProject\".\"Bill\"";
        List<Map<String, Object>> bills = jdbcTemplate.queryForList(sql);
        return bills;
    }

    @PutMapping("/bill")
    public Object editBill(@RequestBody BillChangeMoney bill){
        Object finishMoneyBillFrom = 0;
        Object moneyBillFrom = getMoneyBill(bill.getBillFrom());
        Object finishMoneyBillTo = 0;

        switch (bill.getAssignment()) {
            case "Снятие денег":
            case "Покупка":
                finishMoneyBillFrom =Double.parseDouble(moneyBillFrom.toString()) - bill.getMoney();
                break;
            case "Пополнение баланса":
                finishMoneyBillFrom = Double.parseDouble(moneyBillFrom.toString()) + bill.getMoney();
                break;
            case "Перевод между счетами":
                finishMoneyBillFrom =Double.parseDouble(moneyBillFrom.toString()) - bill.getMoney();
                Object moneyBillTo = getMoneyBill(bill.getBillTo());
                finishMoneyBillTo = Double.parseDouble(moneyBillTo.toString()) + bill.getMoney();
                updateMoneyBill(bill.getBillTo(), finishMoneyBillTo);
                break;
        }
        return updateMoneyBill(bill.getBillFrom(),finishMoneyBillFrom);
    }

    public Object getMoneyBill(Integer bill){
        String sql = "SELECT * FROM \"itProject\".\"Bill\" WHERE \"ID\" = ?";
        Map<String, Object> billSql = jdbcTemplate.queryForMap(sql, bill);
        return billSql.get("money");
    }

    public int updateMoneyBill(Integer bill, Object money){
        return jdbcTemplate.update(
                "UPDATE \"itProject\".\"Bill\" SET money = ? WHERE \"ID\"=?",
                money, bill);
    }
}
