package testProject.models;

public class BillChangeMoney {
    private int billFrom;
    private int billTo;
    private double money;
    private String assignment;

    public BillChangeMoney(int billFrom, int billTo, double money, String assignment) {
        this.billFrom = billFrom;
        this.billTo = billTo;
        this.money = money;
        this.assignment = assignment;
    }

    public int getBillFrom() {
        return billFrom;
    }

    public void setBillFrom(int billFrom) {
        this.billFrom = billFrom;
    }

    public int getBillTo() {
        return billTo;
    }

    public void setBillTo(int billTo) {
        this.billTo = billTo;
    }

    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }

    public String getAssignment() {
        return assignment;
    }

    public void setAssignment(String assignment) {
        this.assignment = assignment;
    }
}