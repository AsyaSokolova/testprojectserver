package testProject.models;


import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;

@Entity
@Table(name = "Bill")
public class EntityBill {
    private @Id @GeneratedValue Long id;
    @OneToOne
    @JoinColumn(name = "client")
    @JsonProperty("client")
    private EntityClient client;
    private double money;

    public EntityBill() {

    }

    public EntityBill(Long id, EntityClient client, double money) {
        this.id = id;
        this.client = client;
        this.money = money;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public EntityClient getClient() {
        return client;
    }

    public void setClient(EntityClient idClient) {
        this.client = idClient;
    }

    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }
}
