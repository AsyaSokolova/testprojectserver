package testProject.models;

public class Bill {
    private int clientId;
    private double money;

    public Bill(int id, int client, double money) {
        this.clientId = client;
        this.money = money;
    }

    public int getClient() {
        return clientId;
    }

    public void setClient(int client) {
        this.clientId = client;
    }

    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }
}
