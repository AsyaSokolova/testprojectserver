package testProject.models;

import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "Client")
public class EntityClient {
    private @Id @GeneratedValue Long id;
    private String name;
    private int age;

    public EntityClient() {
    }

    public EntityClient(Long id, String name){
        this.id = id;
        this.name = name;
    }

    public EntityClient(Long id, String name, int age){
        this.id = id;
        this.name = name;
        this.age = age;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Id
    @JsonGetter("id")
    public Long getId() {
        return id;
    }

    @JsonGetter("age")
    public int getAge() {
        return age;
    }

    @JsonGetter("name")
    public String getName() {
        return name;
    }

}
