package testProject.models;

import javax.persistence.*;

@Entity
@Table(name = "Transaction")
public class EntityTransaction {
    private @Id @GeneratedValue Long ID;
    @OneToOne
    @JoinColumn
    private EntityBill billFrom;
    @OneToOne
    @JoinColumn
    private EntityBill billTo;
    private double amount;
    private String assignment;

    public EntityTransaction(Long id, EntityBill billFrom, EntityBill billTo, double amount) {
        this.ID = id;
        this.billFrom = billFrom;
        this.billTo = billTo;
        this.amount = amount;
    }

    public EntityTransaction() {

    }

    public Long getId() {
        return ID;
    }

    public void setId(Long id) {
        this.ID = id;
    }

    public EntityBill getBillFrom() {
        return billFrom;
    }

    public void setBillFrom(EntityBill billFrom) {
        this.billFrom = billFrom;
    }

    public EntityBill getBillTo() {
        return billTo;
    }

    public void setBillTo(EntityBill billTo) {
        this.billTo = billTo;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getAssignment() {
        return assignment;
    }

    public void setAssignment(String assignment) {
        this.assignment = assignment;
    }
}
