package testProject.models;

public class Transaction {
    private int billFrom;
    private int billTo;
    private double amount;
    private String assignment;

    public Transaction(int billFrom, int billTo, double amount) {
        this.billFrom = billFrom;
        this.billTo = billTo;
        this.amount = amount;
    }

    public int getBillFrom() {
        return billFrom;
    }

    public void setBillFrom(int billFrom) {
        this.billFrom = billFrom;
    }

    public int getBillTo() {
        return billTo;
    }

    public void setBillTo(int billTo) {
        this.billTo = billTo;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getAssignment() {
        return assignment;
    }

    public void setAssignment(String assignment) {
        this.assignment = assignment;
    }
}
