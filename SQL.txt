-- Table: itProject.Client

-- DROP TABLE "itProject"."Client";

CREATE TABLE "itProject"."Client"
(
    "ID" integer NOT NULL DEFAULT nextval('"itProject"."Client_ID_seq"'::regclass),
    name character varying COLLATE pg_catalog."default" NOT NULL,
    age integer,
    CONSTRAINT "Client_pkey" PRIMARY KEY ("ID")
)

TABLESPACE pg_default;

ALTER TABLE "itProject"."Client"
    OWNER to postgres;

-- DROP TABLE "itProject"."Bill";

CREATE TABLE "itProject"."Bill"
(
    "ID" integer NOT NULL DEFAULT nextval('"itProject"."Bill_ID_seq"'::regclass),
    client integer NOT NULL,
    money numeric NOT NULL,
    CONSTRAINT "Bill_pkey" PRIMARY KEY ("ID"),
    CONSTRAINT client_fkey FOREIGN KEY (client)
        REFERENCES "itProject"."Client" ("ID") MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
)

TABLESPACE pg_default;

ALTER TABLE "itProject"."Bill"
    OWNER to postgres;

-- Table: itProject.Transaction

-- DROP TABLE "itProject"."Transaction";

CREATE TABLE "itProject"."Transaction"
(
    "ID" integer NOT NULL DEFAULT nextval('"itProject"."Transaction_id_seq"'::regclass),
    "billFrom" integer,
    "billTo" integer,
    amount numeric NOT NULL,
    assignment character varying COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT transaction_pk PRIMARY KEY ("ID"),
    CONSTRAINT transaction_bill_id_fk FOREIGN KEY ("billFrom")
        REFERENCES "itProject"."Bill" ("ID") MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT transaction_bill_id_fk_2 FOREIGN KEY ("billTo")
        REFERENCES "itProject"."Bill" ("ID") MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)

TABLESPACE pg_default;

ALTER TABLE "itProject"."Transaction"
    OWNER to postgres;
-- Index: transaction_id_uindex

-- DROP INDEX "itProject".transaction_id_uindex;

CREATE UNIQUE INDEX transaction_id_uindex
    ON "itProject"."Transaction" USING btree
    ("ID" ASC NULLS LAST)
    TABLESPACE pg_default;

INSERT INTO "itProject"."Client"(name, age)
	VALUES ('Иванов Иван Иванович', 25);

INSERT INTO "itProject"."Client"(name, age)
	VALUES ('Петров Петр Петрович', 44);

INSERT INTO "itProject"."Client"(name, age)
	VALUES ('Петров Петр Петрович', 44);
